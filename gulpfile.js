var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;

var proxy = 'localhost/home-i'

gulp.task('serve', function () {
    browserSync.init([
        //'./**/*.php',
        './dist/css/**/*.css',
        './dist/js/**/*.js',
    ], {
            proxy: proxy
        });
});

gulp.task('default', function () {
    console.log('This is default gulp task.');
});
