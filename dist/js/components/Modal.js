
export default class Modal {

    constructor(modal) {
        this.modal = modal;
        this.init();
    }

    init() {
        // 
    }

    show(callback) {
        this.modal.addClass('show');
        if(callback) callback;
    }

    hide(callback) {
        this.modal.removeClass('show');
        if(callback) callback;
    }

}