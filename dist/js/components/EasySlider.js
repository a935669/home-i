
/**
 * 這個 component 依賴 jquery, hammerjs
 * 使用前確保已經引入以上兩套 library.
 */
export default class EasySlider {

    /**
     * container 必須是 jQuery 物件
     * 例如：$('.easy-slider')
     */
    constructor(container) {

        /**
         * 屬性初始化
         */
        this.activeItemIndex = 0;
        this.container = container;
        this.items = container.find('.slider-item');
        this.control = container.find('.slider-control');
        this.controlDots = this.control.find('.control-dot');
        this.prevBtn = this.container.find('.prev-btn');
        this.nextBtn = this.container.find('.next-btn');

        /** 元件初始化 */
        this.init();
    }

    init() {

        let self = this;

        let el = this.container[0];

        let mc = new Hammer(el);

        /** 初始化 slider 的 panleft 事件  */
        mc.on('swipeleft', _.debounce(function () {
            self.moveToNext();
        }, 50));

        /** 初始化 slider 的 panright 事件  */
        mc.on('swiperight', _.debounce(function () {
            self.moveToPrev();
        }, 50));

        /** 如果有 prev btn, 初始化它的事件監聽 */
        if (this.prevBtn.length) {
            this.prevBtn.on('click', () => {
                this.moveToPrev();
            });
        }

        /** 如果有 next btn, 初始化它的事件監聽 */
        if (this.nextBtn.length) {
            this.nextBtn.on('click', () => {
                this.moveToNext();
            });
        }

        /** 直接點擊 "點點" */
        this.controlDots.on('click', function () {
            self.moveTo(self.controlDots.index(this));
        });
    }

    moveToPrev() {

        /** 計算合理的上 prev index */
        let index = this.activeItemIndex ? this.activeItemIndex - 1 : this.items.length - 1;

        /** 移動至該引索 */
        this.moveTo(index);
    }

    moveToNext() {

        /** 計算合理的上 next index */
        let index = this.activeItemIndex === this.items.length - 1 ? 0 : this.activeItemIndex + 1;
        
        /** 移動至該引索 */
        this.moveTo(index);
    }

    moveTo(index) {

        /** 如果有 control dot, 更新 active */
        if (this.controlDots.length) {
            this.controlDots.removeClass('active');
            this.controlDots.eq(index).addClass('active');
        }

        /** 設定當下激活的 slide item */
        this.setCurrent(index);
    }

    setCurrent(index) {

        /** 更新 current active slide item */
        this.activeItemIndex = index;

        /** 移動 */
        this.items.css({
            'transform': 'translateX(-' + index * 100 + '%)'
        });

    }
};