$(function() {
    $('.slider4').bxSlider({
        slideWidth: 370,
        minSlides: 1,
        maxSlides: 5,
        moveSlides: 1,
        slideMargin: 10,
        pager: false
        // auto: true,
        // speed: 500,
        // autoDelay: 0,
        // autoHover: true
    });

    $('.bxslider').bxSlider({
        pagerCustom: '#bx-pager',
        controls: false
    });
});