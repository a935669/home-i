let path = require('path');
let webpack = require('webpack');
let autoprefixer = require('autoprefixer');
let ExtractTextPlugin = require("extract-text-webpack-plugin");
let UglifyJSPlugin = require('uglifyjs-webpack-plugin');

let isProduction = (process.env.NODE_ENV === 'production');
process.noDeprecation = true;

module.exports = {
    entry: {
        app: [
            './src/js/main.js',
            './src/scss/main.scss'
        ],
        vendor: ['jquery', 'lodash', 'hammerjs']
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].js',
        publicPath: './dist/'
    },
    module: {
        rules: [
            {
                test: /\.(s[ac]ss)$/,
                exclude: [/node_modules/],
                use: ExtractTextPlugin.extract({
                    use: [{
                        loader: 'css-loader',
                        options: {
                            url: false
                        }
                    }, 'postcss-loader', 'sass-loader'],
                    fallback: "style-loader",
                    publicPath: './dist/css'
                })
            },
            {
                test: /\.js$/,
                exclude: [/node_modules/],
                use: {
                    loader: 'babel-loader',
                    query: {
                        presets: ['es2015']
                    }
                }
            }
        ]
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },

    plugins: [
        //new UglifyJSPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor']
        }),
        new ExtractTextPlugin({
            filename: "css/style.css",
            disable: false,
            allChunks: true
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: isProduction,
            options: {
                postcss: [
                    autoprefixer({
                        browsers: ['last 1 versions']
                    })
                ]
            }
        }),
        new webpack.ProvidePlugin({
            Promise: "bluebird"
        })
    ]
}